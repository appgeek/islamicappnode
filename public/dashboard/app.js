var app = angular.module("adminApp", ['ngRoute', 'ngDialog', 'ngFileUpload']);

app.config(function ($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl: './components/mosques/mosques.html',
			controller: 'mosquesController'
		})

})

app.controller('adminController', function ($scope, $http, $window) {

	var url = window.location.origin + "/signOut"

	$scope.logout = function () {
		$http.put(url).then(function (response) {
			if (response.status == 204) {
				$window.location.href = '/dashboard/login.html'
			} else {
				console.log(response)
			}
		}).catch(function (err) {
			console.log(err)
		})
	}
})