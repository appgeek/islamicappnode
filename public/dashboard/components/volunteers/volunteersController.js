app.controller('volunteersController', function ($scope, $rootScope, $http, $window, ngDialog, Upload) {

  const url = window.location.origin

  $http.get(url + '/articles/volunteers').then(function (response) {
    $rootScope.volunteers = response.data.articles
  }).catch(function (err) {
    console.log(err)
  })

  $scope.openTab = function (link) {
    $window.open(link, '_blank')
  }

  $scope.openImage = function (link) {
    ngDialog.open({
      template: 'openImage',
      className: 'ngdialog-theme-default',
      width: '1200px',
      controller: ['$scope', function ($scope) {
        $scope.image = link
      }]
    })
  }

  $rootScope.upload = function (file, article, type, loading) {
    loading = true
    Upload.upload({
      url: url + '/upload',
      data: { file: file }
    }).then(function (resp) {

      switch (type) {
        case 'image':
          article.image = resp.data.fileUrl
          break;

        case 'pdf':
          article.pdf = resp.data.fileUrl
          break;

        case 'word':
          article.word = resp.data.fileUrl
          break;
      }

      loading = false
    })
  }

  $scope.add = function () {
    ngDialog.open({
      template: 'add',
      className: 'ngdialog-theme-default',
      width: '600px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        $scope.newArticle = { type: 'volunteers' }
        $scope.loading = false

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          $scope.loading = true
          $http.post(url + '/article', $scope.newArticle).then(function (response) {
            $rootScope.volunteers.push(response.data.article)
            $scope.loading = false
            $scope.closeThisDialog()
          }).catch(function (err) {
            console.log(err)
          })
        }

      }]
    })
  }

  $scope.edit = function (index) {
    ngDialog.open({
      template: 'edit',
      className: 'ngdialog-theme-default',
      width: '600px',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        $scope.currentArticle = angular.copy($rootScope.volunteers[index])
        $scope.loading = false

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          $scope.loading = true

          $http.put(url + '/article/' + $scope.currentArticle._id, $scope.currentArticle).then(function (response) {
            $rootScope.volunteers[index] = $scope.currentArticle
            $scope.loading = false
            $scope.closeThisDialog()
          }).catch(function (err) {
            console.log(err)
          })
        }

      }]
    })
  }

  $scope.delete = function (index) {

    ngDialog.open({
      template: 'delete',
      className: 'ngdialog-theme-default',
      controller: ['$scope', '$rootScope', function ($scope, $rootScope) {

        $scope.loading = false

        $scope.exit = function () {
          $scope.closeThisDialog()
        }

        $scope.ok = function () {
          $scope.loading = true

          $http.delete(url + '/article/' + $rootScope.volunteers[index]._id).then(function (response) {
            $scope.loading = false
            $rootScope.volunteers.splice(index, 1)
            $scope.closeThisDialog()
          }).catch(function (err) {
            console.log(err)
          })
        }

      }]
    })
  }

})
