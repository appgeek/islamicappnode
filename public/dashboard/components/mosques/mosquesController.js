app.controller('mosquesController', function ($scope, $rootScope, $http, $window, ngDialog, Upload) {
  
    const url = window.location.origin
  
    $http.get(url + '/mosques').then(function (response) {
      $rootScope.mosques = response.data.mosques
    }).catch(function (err) {
      console.log(err)
    })
  
    $scope.openTab = function (link) {
      $window.open(link, '_blank')
    }
  
    $scope.openImage = function (link) {
      ngDialog.open({
        template: 'openImage',
        className: 'ngdialog-theme-default',
        width: '1200px',
        controller: ['$scope', function ($scope) {
          $scope.image = link
        }]
      })
    }
  
    $rootScope.upload = function (file, article, type, loading) {
      loading = true
      Upload.upload({
        url: url + '/upload',
        data: { file: file }
      }).then(function (resp) {
  
        switch (type) {
          case 'image':
            article.photo = resp.data.fileUrl
            break;
  
          case 'pdf':
            article.pdf = resp.data.fileUrl
            break;
  
          case 'word':
            article.word = resp.data.fileUrl
            break;
        }
  
        loading = false
      })
    }
  
    $scope.edit = function (index) {
      ngDialog.open({
        template: 'edit',
        className: 'ngdialog-theme-default',
        width: '600px',
        controller: ['$scope', '$rootScope', function ($scope, $rootScope) {
  
          $scope.currentArticle = $rootScope.mosques[index]
          $scope.loading = false
  
          $scope.exit = function () {
            $scope.closeThisDialog()
          }
  
          $scope.ok = function () {
            $scope.loading = true
  
            $http.put(url + '/updateMosque/' + $scope.currentArticle._id, $scope.currentArticle).then(function (response) {
              // $rootScope.mosques[index] = $scope.currentArticle
              $http.get(url + '/mosques').then(function (response) {
                $rootScope.mosques = response.data.mosques
                $scope.loading = false
                $scope.closeThisDialog()
              }).catch(function (err) {
                console.log(err)
              })
              
            }).catch(function (err) {
              console.log(err)
            })
          }
  
        }]
      })
    }
  
  })
  