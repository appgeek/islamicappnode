import cron from 'cron'
import { Topic } from '../models'
import firebase from "firebase-admin"

firebase.initializeApp({
  credential: firebase.credential.cert({
    "type": "service_account",
    "project_id": "islamicapp-5e9d2",
    "private_key_id": "04b70d5649a7974777784eeff14a98fb1eb94615",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDoCJwOhHrjbCKm\nKu9csBadVuPqilIVeTUsX+IikQAhZCi6c8xAbb4BOn7kzeeQKg4HKMZnKZOYJA4G\n5Q2/H6NNoJ9nyFvGei/MQMeHEcvEHA4QZThjZl7LYCVzlF+iCtSwz3jCc+W0tTL9\n3fHfiMWiDzHmWBza8vxqITkRg3CO5uk0NtNUff3/KzqeW7QiDTXyBxBFphhmiH1P\nAzvjWKQifyoM6ijYb69NqUKsMoMk+a3mlb1g0tJ0Kr0dnyaxGOwz3tg7l/bKWZHL\nzi+84XbeyRfjYZdFqv6nR5sD7JPpL+kukYzsroI35nOL7HBybqjo25gnTqDEREj5\niYNSTFKPAgMBAAECggEAavehHc40Ksislocfjs68ti8a5gJ7Qdv9R+YyBTZsZXhb\nkE0W4bLVz5IscoV71YmVqvZAtMlFefNRJhuNQUkwCvbb1cfXi0HD7DkG4r0w/0jh\nsOxA2Q4V8FL0IPrLNZQhphsXNCmiwTJxffhF64ONfJ7dpa+EKemvtKt5OBoslyt6\ng2iogbPbyWgA1iCsBfLg+43bpwptpYwW3S10UJC1pRBkeyY/iglgYwveotS3gHU+\nGeMBSOBSa0zqmYZe+PEXKAZFSER6FfdBH+kETTtcG9YNHTixePTSwLQTQcY/Fsir\n1vTwoaSTmWj7kNjGylbJ+dKb0ExdJ2DRhdWwXegqkQKBgQD84+jWw6B/rIN+0dYd\njP3zDmM0f4eaZf+MzXkOt6TPQq1RS/37strZI5+Uw6CwzbL7TRnsC3y4o1KJm5qT\nH6qdD/K1CGrd6pIMPl5p5/dGsgkRpXXwWZ2TCH0d5B8PGTV68JCF0bafUGzYBvAM\nZgQ1CoHc/18FejfBmfuhO4McfwKBgQDq4wtFzUogK13BWQsO/tmKgq5pBmA0lL5v\nvkcrZ2NmrBtUxMe8OqAmK344QTs7+NWzhroiHEawitS4MjzVxHcS7qWmTqLPR5dv\nU1nRDoYDhvoRJzfIergm00FEnNxEi9rWOthn67TmBKxiUDcm7wnGDwqshOZAoZda\nKIoZHj4B8QKBgQCu0frvJHN6b07Dx9mFuib/9E0+SH78jFBaDN0IAIntQYHA9pCQ\nhDbn7slttMMMFlcJhGZMGDDTDwyK1kfINxbh1tmIrlX6LSNjVWY9Pj9h3seILGLY\nkYrtzut3U3DpJhsjbtPpvmzJu3ZS7JDbcVatOCNIHD2a8r9Clekur45WjwKBgQCy\ndqNf+6RwYMKxXvg6AoRC6xudDI0YJlOzDz1/yuQJZIa3l3OynlCD79PM5jMdU+j6\nqEjQFe/XvwP0l6cFPGrEZbJSc7jcCKcUnJ3Ha2ODVpC1a7jEiAlW5nfWq9/n7OLu\nlnNKMhqWEcLwR8Y/vKsOYutXPPq87InUpKyYfRPb4QKBgQCuoCSXCXF+xRvVmqzu\n0xaBD982QQPaeXo2/WtRNtcNfyOwhGZHc48vGulZMtyHL4tY0fdXc9jItq292Foj\nY9rmvdlEERO5ntrpAS4WSIBgR0b8xp3wv8ETuVEYsZxs352WnB6dYYfFhtDgQSHO\nEML01KlIZ4WsHv4mLP8XiaoYug==\n-----END PRIVATE KEY-----\n",
    "client_email": "firebase-adminsdk-4zgas@islamicapp-5e9d2.iam.gserviceaccount.com",
    "client_id": "104407073069776986993",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://accounts.google.com/o/oauth2/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-4zgas%40islamicapp-5e9d2.iam.gserviceaccount.com"
  }
  )
})

const FCM = firebase.messaging()

export async function sendNotifications(req, res) {
  try {

    let registrationTokens = topic.subscriptions.filter(it => { if (it.fcmToken) return it.fcmToken })
    registrationTokens = registrationTokens.map(it => { return it.fcmToken })

    const payload = {
      notification: {
        title: "مبادرتك " + topic.name + "بانتظارك لانجازها",
        body: "خطوة يوميه للوصول لهدفك"
      }
    }

    // console.log(registrationTokens)

    if (registrationTokens.length > 0) {
      const response = await FCM.sendToDevice(registrationTokens, payload)
      console.log(response.results)
    }

  } catch (error) {
    console.log("Error sending message:", error);
  }
}
