import { PORT, NODE_ENV, SECRET } from './config/env'
import session from 'express-session'
import connectMongo from 'connect-mongo'
import routes from './config/routes'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'
import express from 'express'
import morgan from 'morgan'
import https from 'https'
import http from 'http'
import path from 'path'
import fs from 'fs'
import './config/database'

const app = express()
const mongoStore = connectMongo(session)
const server = NODE_ENV == 'development' ? http.createServer(app) : https.createServer({
  key: fs.readFileSync('/etc/letsencrypt/live/chikh.io/privkey.pem'),
  cert: fs.readFileSync('/etc/letsencrypt/live/chikh.io/fullchain.pem')
}, app)

let sess = {
  secret: SECRET,
  resave: false,
  saveUninitialized: true,
  cookie: {},
  store: new mongoStore({ mongooseConnection: mongoose.connection })
}

if (NODE_ENV == 'production')
  sess.cookie.secure = true

app.use(session(sess))
app.use(bodyParser.json({ limit: "4mb" }))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(morgan('dev'))
app.use('/', routes)
app.use('/', express.static(path.join(__dirname, '/../public/uploads')));
// app.use('/', express.static(path.join(__dirname, '/../public/client')));
app.use('/dashboard', express.static(path.join(__dirname, '/../public/dashboard')));

server.listen(PORT, () => console.log('start in ' + NODE_ENV + ' environment on port ' + PORT))
