export const NODE_ENV = process.env.NODE_ENV || 'development'
export const DB = process.env.MONGODB_URI || NODE_ENV == 'development' ? 'mongodb://localhost:27017/islamicApp' : 'mongodb://root:uZp7cO$1$209@localhost:27017/islamicApp'
export const PORT = process.env.PORT || NODE_ENV == 'development' ? 3001 : 443
export const SECRET = process.env.SECRET || 'thisisaverysecurestringtonotbeinghacked'
export const GOOGLE_API = process.env.GOOGLE_API || 'AIzaSyA77TVDtSWwqOILwvWGPMaVJV2qKnxCZ-E'
