import express from 'express'
const router = express.Router()

import staticRoute from '../components/static/staticRoute'
import uploadRoute from '../components/upload/uploadRoute'
import authRoute from '../components/authorization/authRoute'
import mosqueRoute from '../components/mosque/mosqueRoute'

staticRoute(router)
uploadRoute(router)
authRoute(router)
mosqueRoute(router)

export default router
