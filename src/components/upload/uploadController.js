import multer from 'multer'
import path from 'path'

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, path.join(__dirname, '../../../public/uploads'))
  },
  filename: (req, file, cb) => {
    const name = Date.now() + '_' + file.originalname
    cb(null, name)
  }
})

const upload = multer({
  storage: storage,
  limits: { fileSize: 10485760 }
}).single('file')

export default async (req, res) => {
  upload(req, res, err => {
    try {
      if (err) {
        console.log(err)
        return res.status(500).json(err)
      }

      return res.json({ fileUrl: req.protocol + '://' + req.headers.host + '/' + req.file.filename })
    } catch (err) {
      console.log(err)
      res.status(500).end()
    }
  })

}
