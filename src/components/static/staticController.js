
export async function login(req, res, next) {

  if (req.session.token)
    return res.redirect('/dashboard/')
  else
    return next()
}
