import bcrypt from 'bcrypt'
import mongoose, { Schema } from 'mongoose'
import mongooseDelete from 'mongoose-delete'

const mosqueSchema = new Schema(
  {
    name: { type: String },
    photo: { type: String },
    location: { lat: { type: String }, lng: { type: String } },
    googleId: { type: String },
    comments: [{ date: { type: Date }, text: { type: String } }],
    woman: { type: Boolean, default: true },
    hotWater: { type: Boolean, default: true },
    airconditioner: { type: Boolean, default: true },
    mosque: { type: Boolean, default: true },
    imamKhomsRating: [{ date: { type: Date }, rate: { type: Number } }],
    imamJoumouaRating: [{ date: { type: Date }, rate: { type: Number } }],
    cleanlinessRating: [{ date: { type: Date }, rate: { type: Number } }],
    suppliesRating: [{ date: { type: Date }, rate: { type: Number } }],
    structureRating: [{ date: { type: Date }, rate: { type: Number } }],
    supervisorRating: [{ date: { type: Date }, rate: { type: Number } }],
    fcmToken: { type: String }
  },
  {
    timestamps: true
  }
)

mosqueSchema.plugin(mongooseDelete, { overrideMethods: 'all', deletedAt: true, deletedBy: true })

delete mongoose.models.mosque
export default mongoose.model('mosque', mosqueSchema)
