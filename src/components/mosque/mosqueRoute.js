import { getNearBy, getOne, nearMosque, comment, rate, getAll, update, notification } from './mosqueController'

export default function (router) {
  router.put('/mosques', getNearBy)
  router.get('/mosque/:id', getOne)
  router.get('/mosques', getAll)
  router.put('/position', nearMosque)
  router.put('/comment/:id', comment)
  router.put('/rate/:id', rate)
  router.put('/updateMosque/:id', update)
  router.get('/notification', notification)
}
