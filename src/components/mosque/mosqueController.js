import _ from 'lodash'
import geolib from 'geolib'
import request from 'superagent'
import firebase from "firebase-admin"
import { Mosque } from '../../config/models'

const BASE_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="
const END_URL = "&radius=1000&types=mosque&key=AIzaSyA77TVDtSWwqOILwvWGPMaVJV2qKnxCZ-E"

firebase.initializeApp({
  credential: firebase.credential.cert({
    "type": "service_account",
    "project_id": "islamicapp-5e9d2",
    "private_key_id": "04b70d5649a7974777784eeff14a98fb1eb94615",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDoCJwOhHrjbCKm\nKu9csBadVuPqilIVeTUsX+IikQAhZCi6c8xAbb4BOn7kzeeQKg4HKMZnKZOYJA4G\n5Q2/H6NNoJ9nyFvGei/MQMeHEcvEHA4QZThjZl7LYCVzlF+iCtSwz3jCc+W0tTL9\n3fHfiMWiDzHmWBza8vxqITkRg3CO5uk0NtNUff3/KzqeW7QiDTXyBxBFphhmiH1P\nAzvjWKQifyoM6ijYb69NqUKsMoMk+a3mlb1g0tJ0Kr0dnyaxGOwz3tg7l/bKWZHL\nzi+84XbeyRfjYZdFqv6nR5sD7JPpL+kukYzsroI35nOL7HBybqjo25gnTqDEREj5\niYNSTFKPAgMBAAECggEAavehHc40Ksislocfjs68ti8a5gJ7Qdv9R+YyBTZsZXhb\nkE0W4bLVz5IscoV71YmVqvZAtMlFefNRJhuNQUkwCvbb1cfXi0HD7DkG4r0w/0jh\nsOxA2Q4V8FL0IPrLNZQhphsXNCmiwTJxffhF64ONfJ7dpa+EKemvtKt5OBoslyt6\ng2iogbPbyWgA1iCsBfLg+43bpwptpYwW3S10UJC1pRBkeyY/iglgYwveotS3gHU+\nGeMBSOBSa0zqmYZe+PEXKAZFSER6FfdBH+kETTtcG9YNHTixePTSwLQTQcY/Fsir\n1vTwoaSTmWj7kNjGylbJ+dKb0ExdJ2DRhdWwXegqkQKBgQD84+jWw6B/rIN+0dYd\njP3zDmM0f4eaZf+MzXkOt6TPQq1RS/37strZI5+Uw6CwzbL7TRnsC3y4o1KJm5qT\nH6qdD/K1CGrd6pIMPl5p5/dGsgkRpXXwWZ2TCH0d5B8PGTV68JCF0bafUGzYBvAM\nZgQ1CoHc/18FejfBmfuhO4McfwKBgQDq4wtFzUogK13BWQsO/tmKgq5pBmA0lL5v\nvkcrZ2NmrBtUxMe8OqAmK344QTs7+NWzhroiHEawitS4MjzVxHcS7qWmTqLPR5dv\nU1nRDoYDhvoRJzfIergm00FEnNxEi9rWOthn67TmBKxiUDcm7wnGDwqshOZAoZda\nKIoZHj4B8QKBgQCu0frvJHN6b07Dx9mFuib/9E0+SH78jFBaDN0IAIntQYHA9pCQ\nhDbn7slttMMMFlcJhGZMGDDTDwyK1kfINxbh1tmIrlX6LSNjVWY9Pj9h3seILGLY\nkYrtzut3U3DpJhsjbtPpvmzJu3ZS7JDbcVatOCNIHD2a8r9Clekur45WjwKBgQCy\ndqNf+6RwYMKxXvg6AoRC6xudDI0YJlOzDz1/yuQJZIa3l3OynlCD79PM5jMdU+j6\nqEjQFe/XvwP0l6cFPGrEZbJSc7jcCKcUnJ3Ha2ODVpC1a7jEiAlW5nfWq9/n7OLu\nlnNKMhqWEcLwR8Y/vKsOYutXPPq87InUpKyYfRPb4QKBgQCuoCSXCXF+xRvVmqzu\n0xaBD982QQPaeXo2/WtRNtcNfyOwhGZHc48vGulZMtyHL4tY0fdXc9jItq292Foj\nY9rmvdlEERO5ntrpAS4WSIBgR0b8xp3wv8ETuVEYsZxs352WnB6dYYfFhtDgQSHO\nEML01KlIZ4WsHv4mLP8XiaoYug==\n-----END PRIVATE KEY-----\n",
    "client_email": "firebase-adminsdk-4zgas@islamicapp-5e9d2.iam.gserviceaccount.com",
    "client_id": "104407073069776986993",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://accounts.google.com/o/oauth2/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-4zgas%40islamicapp-5e9d2.iam.gserviceaccount.com"
  }
  )
})

const FCM = firebase.messaging()

export async function getNearBy(req, res) {
  try {
    let result = await request.post(BASE_URL + req.body.lat + ',' + req.body.lng + END_URL)
    result = JSON.parse(result.text).results

    let response = []

    for (let mosque of result) {
      let exist = await Mosque.findOne({ googleId: mosque.id })

      if (!exist) {
        exist = await Mosque.create({
          name: mosque.name,
          googleId: mosque.id,
          location: mosque.geometry.location
        })
      }

      let rate = "-1"

      if (exist.imamJoumouaRating.length > 0) {
        const imamKhomsRating = exist.imamKhomsRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / exist.imamKhomsRating.length
        const imamJoumouaRating = exist.imamJoumouaRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / exist.imamJoumouaRating.length
        const cleanlinessRating = exist.cleanlinessRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / exist.cleanlinessRating.length
        const suppliesRating = exist.suppliesRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / exist.suppliesRating.length
        const structureRating = exist.structureRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / exist.structureRating.length
        const supervisorRating = exist.supervisorRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / exist.supervisorRating.length

        rate = (imamJoumouaRating + imamKhomsRating + cleanlinessRating + suppliesRating + structureRating + supervisorRating) / 6
        rate = rate.toFixed(1)
      }

      exist = _.pick(exist, '_id', 'name', 'location')
      exist.rate = rate

      response.push(exist)
    }

    res.json({ mosques: response })

  } catch (error) {
    console.log(error)
    return res.status(500).end()
  }
}

export async function getOne(req, res) {
  try {
    let mosque = await Mosque.findOne({ _id: req.params.id })
    mosque = mosque.toObject()

    let rate = "-1"

    if (mosque.imamJoumouaRating.length > 0) {
      mosque.imamJoumouaRating = mosque.imamJoumouaRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / mosque.imamJoumouaRating.length
      mosque.imamKhomsRating = mosque.imamKhomsRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / mosque.imamKhomsRating.length
      mosque.cleanlinessRating = mosque.cleanlinessRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / mosque.cleanlinessRating.length
      mosque.suppliesRating = mosque.suppliesRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / mosque.suppliesRating.length
      mosque.structureRating = mosque.structureRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / mosque.structureRating.length
      mosque.supervisorRating = mosque.supervisorRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / mosque.supervisorRating.length

      rate = ((mosque.imamJoumouaRating + mosque.imamKhomsRating + mosque.cleanlinessRating +
        mosque.suppliesRating + mosque.structureRating + mosque.supervisorRating) / 6).toFixed(1)

      mosque.imamJoumouaRating = mosque.imamJoumouaRating.toFixed(1)
      mosque.imamKhomsRating = mosque.imamKhomsRating.toFixed(1)
      mosque.cleanlinessRating = mosque.cleanlinessRating.toFixed(1)
      mosque.suppliesRating = mosque.suppliesRating.toFixed(1)
      mosque.structureRating = mosque.structureRating.toFixed(1)
      mosque.supervisorRating = mosque.supervisorRating.toFixed(1)

    }

    mosque.rate = rate
    res.json({ mosque })

  } catch (error) {
    console.log(error)
    return res.status(500).end()
  }
}

export async function update(req, res) {
  try {
    let mosque = await Mosque.findOne({ _id: req.params.id })

    mosque.name = req.body.name
    mosque.photo = req.body.photo
    mosque.woman = req.body.woman
    mosque.hotWater = req.body.hotWater
    mosque.airconditioner = req.body.airconditioner
    mosque.mosque = req.body.mosque

    await mosque.save()
    res.json({ mosque })

  } catch (error) {
    console.log(error)
    return res.status(500).end()
  }
}

export async function getAll(req, res) {
  try {
    const mosques = await Mosque.find({})
    res.json({ mosques })

  } catch (error) {
    console.log(error)
    return res.status(500).end()
  }
}

export async function nearMosque(req, res) {
  try {        
    let mosques = await Mosque.find()

    for (let mosque of mosques) {
      const distance = geolib.getDistance(
        { latitude: mosque.location.lat, longitude: mosque.location.lng },
        { latitude: req.body.lat, longitude: req.body.lng }
      )

      if (distance <= 50)
        return res.json({ near: true, mosque: mosque._id, distance: distance })
    }

    return res.json({ near: false })

  } catch (error) {
    console.log(error)
    return res.status(500).end()
  }
}

export async function comment(req, res) {
  try {
    let mosque = await Mosque.findOne({ _id: req.params.id })
    mosque.comments.push({ date: new Date(), text: req.body.comment })
    await mosque.save()

    return res.json({ comment: mosque.comments.pop(0) })

  } catch (error) {
    console.log(error)
    return res.status(500).end()
  }
}

export async function rate(req, res) {
  try {
    let mosque = await Mosque.findOne({ _id: req.params.id })

    mosque.imamKhomsRating.push({ date: new Date(), rate: req.body.imamKhomsRating })
    mosque.imamJoumouaRating.push({ date: new Date(), rate: req.body.imamJoumouaRating })
    mosque.cleanlinessRating.push({ date: new Date(), rate: req.body.cleanlinessRating })
    mosque.suppliesRating.push({ date: new Date(), rate: req.body.suppliesRating })
    mosque.structureRating.push({ date: new Date(), rate: req.body.structureRating })
    mosque.supervisorRating.push({ date: new Date(), rate: req.body.supervisorRating })

    mosque.comments.push({ date: new Date(), text: req.body.comment })

    await mosque.save()

    mosque = mosque.toObject()

    mosque.imamJoumouaRating = mosque.imamJoumouaRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / mosque.imamJoumouaRating.length
    mosque.imamKhomsRating = mosque.imamKhomsRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / mosque.imamKhomsRating.length
    mosque.cleanlinessRating = mosque.cleanlinessRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / mosque.cleanlinessRating.length
    mosque.suppliesRating = mosque.suppliesRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / mosque.suppliesRating.length
    mosque.structureRating = mosque.structureRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / mosque.structureRating.length
    mosque.supervisorRating = mosque.supervisorRating.map(it => it.rate).reduce((prevVal, elem) => prevVal + elem, 0) / mosque.supervisorRating.length

    mosque.rate = ((mosque.imamJoumouaRating + mosque.imamKhomsRating + mosque.cleanlinessRating +
      mosque.suppliesRating + mosque.structureRating + mosque.supervisorRating) / 6).toFixed(1)

    mosque.imamJoumouaRating = mosque.imamJoumouaRating.toFixed(1)
    mosque.imamKhomsRating = mosque.imamKhomsRating.toFixed(1)
    mosque.cleanlinessRating = mosque.cleanlinessRating.toFixed(1)
    mosque.suppliesRating = mosque.suppliesRating.toFixed(1)
    mosque.structureRating = mosque.structureRating.toFixed(1)
    mosque.supervisorRating = mosque.supervisorRating.toFixed(1)

    return res.json({ mosque, comment: mosque.comments.pop(0) })

  } catch (error) {
    console.log(error)
    return res.status(500).end()
  }
}

export async function notification(req, res) {
  try {

    let hadiths = [
      "إنما الأعمال بالنيات، وإنما لكلِّ امرئ ما نوى، فمن كانت هجرته إلى الله ورسوله فهجرته إلى الله ورسوله، ومن كانت هجرته لدنيا يصيبها أو امرأة ينكحها فهجرته إلى ما هاجر إليه",
      "أبْغَضُ الرجال إلى الله الألَدُّ الخَصِم",
      "اتق الله حيثما كنت، وأتبع السيئة الحسنة تمحها، وخالق الناس بخلق حسن",
      "صلاة الجماعة تفضل صلاة الفذِّ بسبع وعشرين درجة",
      "من صلى العشاء في جماعة فكأنما قام نصف الليل، ومن صلى الصبح في جماعة فكأنما صلى الليل كله",
      "يسروا ولا تعسروا وبشروا ولا تنفروا"
    ]

    let registrationTokens = "e4YSBPunSIY:APA91bFnPMLJesvtqwE-wIeauZAnKpPtX2u5VPSmq5GbzxP4ZbBpE5Mn-vpgbcsVDjzCoRFILNTKiqZTmb-HL14w1Rm5WRbNw3n4-I-xV5aFMlmSY1zmSaoRxHfDN8lYX7XLNRPyC-MU"

    const payload = {
      notification: {
        title: "حديث من السيرة النبوية",
        body: hadiths[Math.floor(Math.random() * hadiths.length)]
      }
    }

    const response = await FCM.sendToDevice(registrationTokens, payload)
    console.log(response.results)

    return res.json({ response: response.results })

  } catch (error) {
    console.log(error)
    return res.status(500).end()
  }
}
